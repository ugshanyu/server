const Pool = require("pg").Pool;

const pool = new Pool({
 host: "localhost",
 port: 5432,
 user: "postgres",
 password: "Sw13d027",
 database: "firstPgDataBase"
});

module.exports = pool;