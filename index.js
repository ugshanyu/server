const express = require("express");
const app = express();
const cors = require("cors");
const pool = require("./db");
const jwt = require('jsonwebtoken');

app.use(cors());
app.use(express.json());

app.post("/newUser", async(req, res) => {
	const {token} = req.body;
	jwt.verify(token, 'supersecret', async function(err, decoded){
		try{
			if(decoded==null){
				res.json({auth:"failed",data:null})
			}
			else{
			const checkUser = await pool.query("SELECT * FROM users WHERE user_id = $1",[decoded.userId]);
			if(!err&& checkUser.rows.length==1 && checkUser.rows[0].role=="admin"){
				const {email, regNo, userPassword} = req.body;
				const newUser = await pool.query("INSERT INTO users (email,reg_no, user_password) VALUES($1,$2,crypt(($3), gen_salt('bf'))) RETURNING *", 
					[email, regNo, userPassword]); 
				res.json({response:"succeeded"})

			} else {
				res.json({response:"auth failed"})
			}}
		} catch (err){
			console.log(err);
		}
	})})

app.post("/authenticateUser", async(req, res) => {
	try{
		const {email, regNo, userPassword} = req.body;
		const newUser = await pool.query("SELECT * FROM users WHERE EMAIL = $1 and user_password = crypt($2, user_password);", 
			[email,userPassword]);
		if(newUser.rows.length==1){
			var token = jwt.sign({userId:newUser.rows[0].user_id}, 'supersecret',{expiresIn: 1200});
			res.json({auth : true, token : token, role : newUser.rows[0].role});
		} else {
			res.json({auth:false,token:""})
		}
	} catch (err) {
		console.log(err.message);
	}
});

app.post("/Users", async(req, res) => {
	const {token} = req.body;
	jwt.verify(token, 'supersecret', async function(err, decoded){
		try{
			if(decoded==null){
				res.json({auth:"failed",data:null})
			} else {
				const checkUser = await pool.query("SELECT * FROM users WHERE user_id = $1",[decoded.userId]);
				console.log(checkUser)
				if(err){
					console.log(err);
					res.json({auth:"failed",data:null})
				} else if(checkUser.rows.length==1 && checkUser.rows[0].role=="admin") {
					const newUser = await pool.query("SELECT user_id,email,reg_no FROM users");
					res.json({auth:"succeeded",data:newUser.rows});
				} else{
					res.json({auth:"failed",data:newUser.rows});
				}
			}
		} catch (err){
			console.log(err);
			res.json({auth:"failed",data:null})
		}
		
	});

	});

app.post("/myInfo", async(req, res) => {
	const {token} = req.body;

	jwt.verify(token, 'supersecret', async function(err, decoded){
		try{
			if(!err && decoded!=null){
				const queryResponse = await pool.query("SELECT * from users where user_id = $1",[decoded.userId]);
				if(queryResponse.rowCount==1){
					res.json({auth:"succeeded",email:queryResponse.rows[0].email,regNo:queryResponse.rows[0].reg_no});
				} else {
					res.json({response:"failed"});
				}
			} else {
				res.json({auth:"failed"});
			}
		} catch (err){
			console.log(err);
		}

		
	});
});

app.post("/checkToken", async(req, res) => {
	const {token} = req.body;

	jwt.verify(token, 'supersecret', async function(err, decoded){
		try{
			if(!err){
				res.json({response:"succeeded"});
			} else {
				res.json({response:"failed"});
			}
		} catch (err){
			res.json({response:"failed"});
			console.log(err);
			console.log("")
		}
	});
});

app.post("/exit", async(req, res) => {
	const {token} = req.body;

	jwt.verify(token, 'supersecret', async function(err, decoded){
		try{
			if(!err && decoded!=null){
				const queryResponse = await pool.query("SELECT * from users where user_id = $1",[decoded.userId]);
				if(queryResponse.rowCount==1){
					res.json({auth:"succeeded",email:queryResponse.rows[0].email,regNo:queryResponse.rows[0].reg_no});
				} else {
					res.json({response:"failed"});
				}
			} else {
				res.json({auth:"failed"});
			}
		} catch (err){
			console.log(err);
		}

		
	});
});

app.post("/updateMyInfo", async(req, res) => {
	const {email, regNo, userPassword,token} = req.body;
	jwt.verify(token, 'supersecret', async function(err, decoded){
		try{
			if(!err && decoded!=null){
				var queryResponse = {};
				if(userPassword==""){
					queryResponse = await pool.query("UPDATE users SET email = $1, reg_no = $2 WHERE user_id = $3;",[email,regNo,decoded.userId]);
				} else{
					queryResponse = await pool.query("UPDATE users SET email = $1, reg_no = $2, user_password = crypt(($3), gen_salt('bf')) WHERE user_id = $4;",[email,regNo,userPassword,decoded.userId]);
				}
				if(queryResponse.rowCount==1){
					res.json({auth:"succeeded", response:"succeeded"});
				} else {
					res.json({auth:"succeeded",response:"failed"});
				}
			} else {
				res.json({auth:"failed",response:"failed"});
			}
		} catch (err){
			console.log(err);
		}

		
	});
});

app.post("/updateUserInfo", async(req, res) => {
	const {email, regNo, userPassword, userId,token} = req.body;

	jwt.verify(token, 'supersecret', async function(err, decoded){
		try{
			if(!err && decoded!=null){
				var queryResponse = {};
				if(userPassword==null){
					queryResponse = await pool.query("UPDATE users SET email = $1, reg_no = $2 WHERE user_id = $3;",[email,regNo,userId]);
				} else{
					queryResponse = await pool.query("UPDATE users SET email = $1, reg_no = $2, user_password = crypt(($3), gen_salt('bf')) WHERE user_id = $4;",[email,regNo,userPassword,userId]);
					console.log(queryResponse);
				}
				if(queryResponse.rowCount==1){
					res.json({auth:"succeeded", response:"succeeded"});
				} else {
					res.json({auth:"succeeded",response:"failed"});
				}
			} else {
				res.json({auth:"failed",response:"failed"});
			}
		} catch (err){
			console.log(err);
		}

		
	});
});

app.post("/delete", async(req, res) => {
	const {id,token} = req.body;

	jwt.verify(token, 'supersecret', async function(err, decoded){
		try{
			if(decoded==null){
				res.json({auth:"failed",data:null})
			}
			else{
			const checkUser = await pool.query("SELECT * FROM users WHERE user_id = $1",[decoded.userId]);
			if(!err && checkUser.rows.length==1 && checkUser.rows[0].role=="admin"){

				const queryResponse = await pool.query("DELETE FROM users where user_id = $1",[id]);
				if(queryResponse.rowCount!=0){
					res.json({deleted:true});
				} else {
					res.json({deleted:false});
				}
				
				
			} else {
				res.json({auth:"failed"});
			}}
		} catch (err){
			console.log(err);
		}

		
	});
});

app.listen(5000, () => {
	console.log("server has started on 5000");
})
